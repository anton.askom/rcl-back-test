require('dotenv').config()
module.exports = {
  apps : [{
    name: "api_"+process.env.PORT,
    script: "npm",
    args: "run prod",
    env: {
      NODE_ENV: "development",
    },
    env_production: {
      NODE_ENV: "production",
    }
  }]
}
